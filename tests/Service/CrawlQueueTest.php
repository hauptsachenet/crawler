<?php

namespace App\Tests\Service;


use App\Entity\CrawlJob;
use App\Entity\Project;
use App\Service\CrawlQueue;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use GuzzleHttp\Psr7\Uri;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CrawlQueueTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected static $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        self::$entityManager = self::$container->get(EntityManagerInterface::class);
        $metadata = self::$entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool(self::$entityManager);
        $schemaTool->dropDatabase();
        $schemaTool->createSchema($metadata);
    }

    public function testPreventDoubleCrawl()
    {
        $crawlQueue = self::$container->get(CrawlQueue::class);
        $entityManager = self::$container->get(EntityManagerInterface::class);
        $entityManager->beginTransaction();

        $project = new Project();
        $project->setName('test');
        $project->setStartingPoint(new Uri('http://www.example.com/'));

        $entityManager->persist($project);
        $entityManager->flush();
        // job gets created by entity listener
        $entityManager->commit();

        $entityManager->beginTransaction();
        $this->assertEquals(1, $crawlQueue->createCrawlJob($project, $project->getStartingPoint())->getId());
        $this->assertEquals(1, $crawlQueue->createCrawlJob($project, $project->getStartingPoint())->getId());
        $entityManager->rollback();
    }

    public function testIgnoreFragments()
    {
        $entityManager = self::$container->get(EntityManagerInterface::class);
        $entityManager->beginTransaction();

        $project = new Project();
        $project->setName('test');
        $project->setStartingPoint(new Uri('http://www.example.com/#fragment'));

        $entityManager->persist($project);
        $entityManager->flush();
        // job gets created by entity listener
        $entityManager->commit();

        /** @var CrawlJob $job */
        $job = self::$entityManager->find(CrawlJob::class, 1);
        $this->assertEquals('', $job->getUri()->getFragment(), "Fragment must be removed");
    }
}
