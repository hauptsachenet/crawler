<?php

namespace App\Tests\Entity;


use App\Entity\Project;
use App\Entity\Result\Reference;
use App\Entity\Result;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class ReferenceTest extends TestCase
{
    public function testContentFragment()
    {
        $result = new Result(new Project(), new Uri('http://www.example.com/'), new Response(200, [], 'hallo welt'));
        $reference = new Reference($result, new Uri('http://www.example.com/'), 'App\\Processor\\Test', [], 6, 4);
        $this->assertEquals('welt', $reference->getBodyFragment());
    }
}
