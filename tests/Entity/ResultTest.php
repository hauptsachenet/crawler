<?php

namespace App\Tests\Entity;


use App\Entity\Project;
use App\Entity\Result;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{
    /**
     * @expectedException \OutOfRangeException
     */
    public function testLineNumbers()
    {
        $result = new Result(new Project(), new Uri('http://www.example.com/'), new Response(200, [], "\n\n\n"));
        $this->assertEquals([0, 0], $result->convertLineNumberToRange(0));
        $this->assertEquals([1, 0], $result->convertLineNumberToRange(1));
        $this->assertEquals([2, 0], $result->convertLineNumberToRange(2));
        $result->convertLineNumberToRange(3);
    }
}
