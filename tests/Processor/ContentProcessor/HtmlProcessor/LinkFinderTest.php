<?php

namespace App\Tests\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Project;
use App\Entity\Result;
use App\Processor\ContentProcessor\HtmlProcessor;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class LinkFinderTest extends TestCase
{
    private $htmlReferenceFinder;
    private $htmlProcessor;

    protected function setUp()
    {
        parent::setUp();
        $this->htmlReferenceFinder = new HtmlProcessor\LinkFinder();
        $this->htmlProcessor = new HtmlProcessor([$this->htmlReferenceFinder]);
    }

    public static function sets()
    {
        return [
            [
                '<a href="http://www.example.com/">Link</a>',
                ['http://www.example.com/'],
                ['<a href="http://www.example.com/">Link</a>'],
            ],
            [
                '<a href="/">Link</a>',
                ['http://www.example.com/'],
                ['<a href="/">Link</a>'],
            ],
            [
                '<a href="link">Link</a>',
                ['http://www.example.com/link'],
                ['<a href="link">Link</a>'],
            ],
            [
                '<a href="?q=search">Link</a>',
                ['http://www.example.com/?q=search'],
                ['<a href="?q=search">Link</a>'],
            ],
            [
                '<a href="#anchor">Link</a>',
                ['http://www.example.com/#anchor'],
                ['<a href="#anchor">Link</a>'],
            ],
            [
                '<a href="mailto:mustermann@example.com">Link</a>',
                [],
                [],
            ],
            [
                '<a href="tel:+4900000000">Link</a>',
                [],
                [],
            ],
            [
                '<a href="Javascript: void 0">Link</a>',
                [],
                [],
            ],
        ];
    }

    /**
     * @dataProvider sets
     */
    public function testReferenceFinding($content, $urls, $names)
    {
        $response = new Response(200, ['Content-Type' => 'text/html'], $content);
        $result = new Result(new Project(), new Uri('http://www.example.com/'), $response);

        $this->htmlProcessor->process($result);

        $references = $result->getReferences();
        $this->assertCount(count($urls), $references);
        foreach ($urls as $index => $url) {
            $this->assertEquals($url, (string)$references[$index]->getUri());
            $this->assertEquals($names[$index], $references[$index]->getBodyFragment());
        }
    }
}
