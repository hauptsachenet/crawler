<?php

namespace App\Tests\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Project;
use App\Entity\Result;
use App\Processor\ContentProcessor\HtmlProcessor;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;

class ImageFinderTest
{
    private $imageFinder;
    private $htmlProcessor;

    protected function setUp()
    {
        parent::setUp();
        $this->imageFinder = new HtmlProcessor\ImageFinder();
        $this->htmlProcessor = new HtmlProcessor([$this->imageFinder]);
    }

    public static function sets()
    {
        return [
            [
                '<img src="http://www.example.com/"/>',
                [
                    ['http://www.example.com/', 'img', 'src', null]
                ],
                ['<img src="http://www.example.com/"/>'],
            ],
            [
                '<img srcset="http://www.example.com/ 1x, http://www.example.com/a 2x"/>',
                [
                    ['http://www.example.com/', 'img', 'srcset', '1x'],
                    ['http://www.example.com/a', 'img', 'srcset', '2x'],
                ],
                ['<img srcset="http://www.example.com/ 1x, http://www.example.com/a 2x"/>'],
            ],
            [
                '<picture><source srcset="http://www.example.com/"/></picture>',
                [
                    ['http://www.example.com/', 'source', 'srcset', null]
                ],
                ['<picture><source srcset="http://www.example.com/"/></picture>'],
            ],
        ];
    }

    /**
     * @dataProvider sets
     */
    public function testReferenceFinding($content, $urls, $names)
    {
        $response = new Response(200, ['Content-Type' => 'text/html'], $content);
        $result = new Result(new Project(), new Uri('http://www.example.com/'), $response);

        $this->htmlProcessor->process($result);

        $references = $result->getReferences();
        $this->assertCount(count($urls), $references);
        foreach ($urls as $index => $url) {
            $this->assertEquals($url[0], (string)$references[$index]->getUri());
            $this->assertEquals($url[1], (string)$references[$index]->getInfo()['tag']);
            $this->assertEquals($url[2], (string)$references[$index]->getInfo()['attr']);
            $this->assertEquals($url[3], (string)$references[$index]->getInfo()['mod']);
            $this->assertEquals($names[$index], $references[$index]->getBodyFragment());
        }
    }

}
