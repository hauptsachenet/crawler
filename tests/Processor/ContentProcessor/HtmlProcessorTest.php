<?php

namespace App\Tests\Processor\ContentProcessor;


use App\Entity\Project;
use App\Entity\Result;
use App\Processor\ContentProcessor\HtmlProcessor;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class HtmlProcessorTest extends TestCase
{

    public static function contentTypes()
    {
        return [
            ['text/html', true],
            ['text/html; charset=utf8', true],
            ['application/xhtml+xml; charset=utf8', true],
            ['text/xhtml+xml; charset=utf8', true],
            ['text/xhtml; charset=utf8', true],
            ['text/plain', false],
            ['text/htmlisch', false],
        ];
    }

    /**
     * @dataProvider contentTypes
     */
    public function testExecution($contentType, $matches)
    {
        $response = new Response(200, ['Content-Type' => $contentType], '<a href="http://www.example.de/">Link</a>');
        $result = new Result(new Project(), new Uri('http://www.example.com/'), $response);

        $htmlProcessor = $this->createMock(HtmlProcessor\HtmlProcessorInterface::class);
        $htmlProcessor->expects($matches ? $this->once() : $this->never())->method('process');

        $processor = new HtmlProcessor([$htmlProcessor]);
        $processor->process($result);
    }

}
