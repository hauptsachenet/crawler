<?php

namespace App\Tests\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Project;
use App\Entity\Result;
use App\Processor\ContentProcessor\NoSniffInspector;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;

class NoSniffInspectorTest extends TestCase
{
    private $noSniffInspector;

    protected function setUp()
    {
        parent::setUp();
        $this->noSniffInspector = new NoSniffInspector();
    }

    public static function sets()
    {
        return [
            'no header' => [
                [],
                Inspection::VERDICT_PROBLEM,
                ['header' => ''],
            ],
            'header but empty' => [
                ['X-Content-Type-Options' => ''],
                Inspection::VERDICT_PROBLEM,
                ['header' => ''],
            ],
            'header garbage' => [
                ['X-Content-Type-Options' => 'abcde'],
                Inspection::VERDICT_PROBLEM,
                ['header' => 'abcde'],
            ],
            'header correct' => [
                ['X-Content-Type-Options' => 'nosniff'],
                Inspection::VERDICT_OK,
                ['header' => 'nosniff'],
            ],
        ];
    }

    /**
     * @dataProvider sets
     */
    public function testProcess($header, $expectedVerdict, $expectedInfo)
    {
        $result = new Result(new Project(), new Uri('http://www.example.com/'), new Response(200, $header, "content"));
        $this->noSniffInspector->process($result);
        $this->assertCount(1, $result->getInspections());
        $this->assertEquals(NoSniffInspector::class, $result->getInspections()[0]->getProcessor());
        $this->assertEquals($expectedVerdict, $result->getInspections()[0]->getVerdict());
        $this->assertEquals($expectedInfo, $result->getInspections()[0]->getInfo());
    }
}
