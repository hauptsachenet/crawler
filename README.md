# Web Crawler project

## basic test usage

- `composer db:start` starts a database using docker on localhost
- `bin/console project:create project-name http://www.example.com` creates the crawling project
- `bin/console jms-job-queue:schedule` runs the job queue (default for 15 minutes)

