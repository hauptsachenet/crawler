<?php

namespace App\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class Sha256Type extends Type
{
    /**
     * The algorithm which run over the stream for the database representation.
     */
    const ALGORITHM = 'sha256';

    /**
     * This is the size of a sha256 in heximal.
     */
    const LENGTH = 256 / 4;

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param mixed[] $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $fieldDeclaration['length'] = self::LENGTH;
        $fieldDeclaration['fixed'] = true;
        return $platform->getBinaryTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if (!preg_match('#^[0-9a-f]{' . self::LENGTH . '}$#', $value)) {
            throw ConversionException::conversionFailedFormat($value, self::ALGORITHM, '64 hexadecimal characters');
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if (!preg_match('#^[0-9a-f]{' . self::LENGTH . '}$#', $value)) {
            throw ConversionException::conversionFailedFormat($value, self::ALGORITHM, '64 hexadecimal characters');
        }

        return parent::convertToPHPValue($value, $platform);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::ALGORITHM;
    }
}
