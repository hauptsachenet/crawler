<?php

namespace App\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;

class UriType extends Type
{
    /**
     * Determining the max length of an url isn't that easy but here are a few factors that played a role:
     * - RFC7230 vaguely suggests 8000 octets
     * - apparently the max length IE accepts is 2047, some say 2083
     * - sitemaps allow 2048 characters in their url tag.
     * - google crawl urls with max 2047 characters but with some problems
     * - 3072 bytes! is the max length of an index in mysql.
     *   You also may want multi column indexes so it shouldn't be longer.
     *
     * @see https://stackoverflow.com/a/219664
     * @see https://stackoverflow.com/a/417184
     */
    const LENGTH = 2048;

    /**
     * {@inheritdoc}
     */
    public function getDefaultLength(AbstractPlatform $platform)
    {
        return self::LENGTH;
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $fieldDeclaration['length'] = self::LENGTH;
        return $platform->getBinaryTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     *
     * @return mixed|string|null
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_string($value)) {
            $value = $this->convertToPHPValue($value, $platform);
        }

        if ($value instanceof UriInterface) {
            return (string)$value;
        }

        if ($value === null) {
            return null;
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), [Uri::class, 'null']);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     *
     * @return Uri|mixed|null
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        try {
            return new Uri($value);
        } catch (\InvalidArgumentException $e) {
            throw new ConversionException("Could not convert $value to Uri.", 1572635094, $e);
        }
    }

    public function getName()
    {
        return 'uri';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

}
