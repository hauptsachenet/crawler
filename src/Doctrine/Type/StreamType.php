<?php

namespace App\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use GuzzleHttp\Psr7\LazyOpenStream;
use Psr\Http\Message\StreamInterface;
use function GuzzleHttp\Psr7\copy_to_stream;
use function GuzzleHttp\Psr7\stream_for;
use function GuzzleHttp\Psr7\try_fopen;

/**
 * This type stores psr StreamInterface's within the local filesystem.
 * The database will just get a binary checksum to minimize footprint.
 */
class StreamType extends Sha256Type
{
    const NAME = 'stream';

    /**
     * The amount of characters of the hex representation of the stream
     * to use for an additional directory in the filename.
     * This is important because having a lot of files in a single directory causes performance problems.
     * I use 3 which would be 12 bits or 4,096 possible folder names.
     * Assuming even distribution, that would mean 16,777,216 files could be stored with 4,096 files in each directory.
     * @see https://stackoverflow.com/a/466596
     */
    const FOLDER_NAME_LENGTH = 3;

    /**
     * To avoid having to checksum large streams very often, I add the checksum as a property onto the stream.
     * PHP sadly has to WeakMap so there isn't a really beautiful implementation possible.
     */
    private const CHECKSUM_PROPERTY = '__stream_type_checksum';

    /**
     * @var string
     */
    private $streamFolder;

    public function setStreamFolder(string $path)
    {
        $this->streamFolder = $path;
    }

    private function checksumToFilename(string $checksum)
    {
        return sprintf(
            "%s/%s/%s",
            $this->streamFolder,
            substr($checksum, 0, self::FOLDER_NAME_LENGTH),
            substr($checksum, self::FOLDER_NAME_LENGTH)
        );
    }

    public static function storeStreamInAdvance(StreamInterface $stream)
    {
        /** @var StreamType $type */
        $type = self::getType(self::NAME);
        $checksum = $type->convertToDatabaseValue($stream);
        return $type->convertToPHPValue($checksum);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform = null)
    {
        if ($value === null) {
            return null;
        }

        if (!$value instanceof StreamInterface) {
            throw ConversionException::conversionFailedInvalidType($value, $this->getName(), [StreamInterface::class]);
        }

        if (property_exists($value, self::CHECKSUM_PROPERTY)) {
            $hash = $value->{self::CHECKSUM_PROPERTY};
        } else {
            $hash = \GuzzleHttp\Psr7\hash($value, self::ALGORITHM);
        }

        $filename = $this->checksumToFilename($hash);

        // create directory if it does not exist.
        // checking before doing actually caused concurrency issues, so just try to create it and ignore if it fails
        @mkdir(dirname($filename), 0777, true);

        // create the file if it does not already exist
        // if it does exist, then the checksum is the same and there is nothing to do here
        // concurrency problems could exist but then 2 processes would have to request the same file
        if (!file_exists($filename)) {
            $value->rewind();
            copy_to_stream($value, stream_for(try_fopen($filename, 'wb')));
            // set the file to only be readable to prevent accidental changing attempts
            chmod($filename, 0444);
        }

        return parent::convertToDatabaseValue($hash, $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform = null)
    {
        $value = parent::convertToPHPValue($value, $platform);
        if ($value === null) {
            return null;
        }

        $filename = $this->checksumToFilename($value);
        $stream = new LazyOpenStream($filename, 'rb');

        // secretly store the checksum within the stream
        // other options would be a global map but that would prevent garbage collection
        // this way is just a lot simpler although it will create warnings in most IDE's
        $stream->{self::CHECKSUM_PROPERTY} = $value;

        return $stream;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }
}
