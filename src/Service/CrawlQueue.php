<?php

namespace App\Service;


use App\Entity\CrawlJob;
use App\Entity\Project;
use App\Entity\Result;
use App\Repository\CrawlJobRepository;
use App\Utility\UriUtility;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\UriInterface;

class CrawlQueue
{
    /**
     * @var CrawlJobRepository
     */
    private $crawlJobRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(CrawlJobRepository $crawlJobRepository, EntityManagerInterface $entityManager)
    {
        $this->crawlJobRepository = $crawlJobRepository;
        $this->entityManager = $entityManager;
    }

    public function shouldCrawl(Project $project, UriInterface $uri): bool
    {
        $baseUrl = $project->getStartingPoint();
        return UriUtility::isSameBaseUri($baseUrl, $uri);
    }

    public function createCrawlJob(Project $project, UriInterface $uri): CrawlJob
    {
        $uri = $uri->withFragment('');
        if (!$this->shouldCrawl($project, $uri)) {
            throw new \RuntimeException("Uri $uri should not be crawled.");
        }

        $crawlJob = new CrawlJob($project, $uri, 'project:crawl');
        $existingJob = $this->crawlJobRepository->findExistingJob($crawlJob);
        return $existingJob ?? $crawlJob;
    }

    public function createPostProcessingJob(Result $result): CrawlJob
    {
        $crawlJob = new CrawlJob($result->getProject(), $result->getUri(), 'project:postprocess');
        foreach ($result->getFurtherJobs() as $furtherJob) {
            $crawlJob->getJob()->addDependency($furtherJob->getJob());
        }

        $crawlJob = new CrawlJob($result->getProject(), $result->getUri(), 'project:crawl');
        $existingJob = $this->crawlJobRepository->findExistingJob($crawlJob);
        return $existingJob ?? $crawlJob;
    }
}
