<?php

namespace App\Service\ProcessorRunner;


use App\Entity\Result;
use App\Processor\ContentProcessor\ContentProcessorInterface;
use Psr\Log\LoggerInterface;

class ContentProcessorRunner
{
    /**
     * @var ContentProcessorInterface[]|iterable
     */
    private $contentProcessors;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(iterable $contentProcessors, LoggerInterface $logger)
    {
        $this->contentProcessors = $contentProcessors;
        $this->logger = $logger;
    }

    public function run(Result $result): void
    {
        foreach ($this->contentProcessors as $contentProcessor) {
            $this->logger->debug(
                "run content processor: " . get_class($contentProcessor),
                [
                    'project' => $result->getProject()->getId(),
                    'uri' => $result->getUri(),
                ]
            );
            $contentProcessor->process($result);
        }
    }
}
