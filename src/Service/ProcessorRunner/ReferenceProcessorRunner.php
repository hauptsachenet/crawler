<?php

namespace App\Service\ProcessorRunner;


use App\Entity\Result;
use App\Processor\ReferenceProcessor\ReferenceProcessorInterface;
use App\Repository\CrawlJobRepository;
use App\Repository\ResultRepository;
use App\Service\CrawlQueue;
use Psr\Log\LoggerInterface;

class ReferenceProcessorRunner
{
    /**
     * @var ReferenceProcessorInterface[]|iterable
     */
    private $referenceProcessors;

    /**
     * @var ResultRepository
     */
    private $resultRepository;

    /**
     * @var CrawlJobRepository
     */
    private $crawlJobRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CrawlQueue
     */
    private $crawlQueue;

    public function __construct(iterable $referenceProcessors, ResultRepository $resultRepository, CrawlJobRepository $crawlJobRepository, CrawlQueue $crawlQueue, LoggerInterface $logger)
    {
        $this->referenceProcessors = $referenceProcessors;
        $this->resultRepository = $resultRepository;
        $this->crawlJobRepository = $crawlJobRepository;
        $this->crawlQueue = $crawlQueue;
        $this->logger = $logger;
    }

    public function run(Result $result): void
    {
        $uris = $this->buildUriMap($result->getReferences());
        $resultMap = $this->buildResultMap($uris);

        foreach ($result->getReferences() as $reference) {
            $referenceResult = $resultMap[(string)$reference->getUri()];
            foreach ($this->referenceProcessors as $referenceProcessor) {
                $this->logger->debug(
                    "run reference processor: " . get_class($referenceProcessor),
                    [
                        'project' => $result->getProject()->getId(),
                        'uri' => $referenceResult->getUri(),
                    ]
                );

                $referenceProcessor->process($reference, $reference->getResult(), $referenceResult);
            }
        }
    }

    /**
     * @param Result\Reference[] $references
     *
     * @return array
     */
    private function buildUriMap(array $references): array
    {
        $uris = [];
        foreach ($references as $reference) {
            if (!$this->crawlQueue->shouldCrawl($reference->getResult()->getProject(), $reference->getUri())) {
                continue;
            }

            $uris[(string)$reference->getUri()] = $reference->getUri();
        }
        return $uris;
    }

    private function buildResultMap(array $uris): array
    {
        $results = $this->resultRepository->findBy(['uri' => $uris]);

        // check if reference processing can be performed
        if (count($results) !== count($uris)) {
            throw new \RuntimeException("Not all References were available yet");
        }

        $mappedResult = [];
        foreach ($results as $result) {
            $mappedResult[(string)$result->getUri()] = $result;
        }

        return $mappedResult;
    }
}
