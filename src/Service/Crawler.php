<?php

namespace App\Service;


use App\Entity\Project;
use App\Entity\Result;
use App\Processor\ContentProcessor\ContentProcessorInterface;
use App\Processor\ReferenceProcessor\ReferenceProcessorInterface;
use App\Repository\ResultRepository;
use App\Service\ProcessorRunner\ContentProcessorRunner;
use App\Service\ProcessorRunner\ReferenceProcessorRunner;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\TransferStats;
use Psr\Log\LoggerInterface;

class Crawler
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CrawlQueue
     */
    private $crawlQueue;
    /**
     * @var ContentProcessorRunner
     */
    private $contentProcessorRunner;
    /**
     * @var ReferenceProcessorRunner
     */
    private $referenceProcessorRunner;

    public function __construct(ClientInterface $client, LoggerInterface $logger, CrawlQueue $crawlQueue, ContentProcessorRunner $contentProcessorRunner, ReferenceProcessorRunner $referenceProcessorRunner)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->contentProcessorRunner = $contentProcessorRunner;
        $this->referenceProcessorRunner = $referenceProcessorRunner;
        $this->crawlQueue = $crawlQueue;
    }

    public function crawl(Project $project, Uri $uri): Result
    {
        $context = ['project' => $project->getId(), 'uri' => (string)$uri];

        $this->logger->debug("start to crawl $uri for project $project", $context);
        $response = $this->client->request('get', $uri, [
            'allow_redirects' => false,
            'http_errors' => false,
            'decode_content' => true,
            'on_stats' => function (TransferStats $transferStats) use (&$stats) {
                $stats = $transferStats;
            },
        ]);
        $this->logger->info("got result of $uri for project $project", $context);

        $result = new Result($project, $uri, $response, $stats);
        $this->contentProcessorRunner->run($result);

        foreach ($result->getReferences() as $reference) {
            if (!$this->crawlQueue->shouldCrawl($project, $reference->getUri())) {
                continue;
            }

            $job = $this->crawlQueue->createCrawlJob($project, $reference->getUri());
            $result->addFurtherJob($job);
        }

        // if other jobs were created during the processing of the content, post processing must be run in another job
        // otherwise we can do post processing right here and now and save a process call
        if (count($result->getFurtherJobs()) > 0) {
            $postJob = $this->crawlQueue->createPostProcessingJob($result);
            $result->addFurtherJob($postJob);
        } else {
            $this->postProcess($result);
        }

        $this->logger->debug("done with $uri for project $project", $context);
        return $result;
    }

    public function postProcess(Result $result): void
    {
        $this->referenceProcessorRunner->run($result);
    }
}
