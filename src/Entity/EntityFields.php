<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait EntityFields
{
    /**
     * The id of an entity.
     * Note that this is a string.
     * This is because php can't handle such big integers but they are still very efficient in mysql.
     *
     * @var string|null
     *
     * @ORM\Column(type="bigint", options={"unsigned"=true})
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * The version field must be a mutable date object (datetime_immutable isn't in the list of supported versions).
     * The getter and setter is adjusted to convert that.
     *
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime")
     * @ORM\Version()
     * @Assert\NotNull(groups={"thisGroupDoesNotExistAndIsOnlyThereToPreventTheAutovalidationToFail"})
     */
    private $updatedAt;

    public function getId(): string
    {
        return $this->id === null ? '0' : $this->id;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->id === null;
    }

    /**
     * @return bool
     */
    public function isPersisted(): bool
    {
        return $this->id !== null;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        if ($this->updatedAt === null) {
            return null;
        }

        return \DateTimeImmutable::createFromMutable($this->updatedAt);
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * UpdateAt is a version field.
     * Updating it using doctrine will result into an optimistic lock exception.
     * Of course, this might be desired to avoid
     *
     * @param \DateTimeInterface|null $updatedAt
     * @internal updateAt is a version field.
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        if ($updatedAt instanceof \DateTimeImmutable) {
            $date = new \DateTime();
            $date->setTimestamp($updatedAt->getTimestamp());
            $updatedAt = $date;
        } else {
            $updatedAt = clone $updatedAt;
        }

        $this->updatedAt = $updatedAt;
    }

    /**
     * Gets a string that uniquely identifies this entity.
     * Normally this is the id but it may be eg. a username.
     *
     * @return string
     */
    public function getReadableIdentification(): string
    {
        return $this->getId() ?: spl_object_hash($this);
    }

    public function __toString(): string
    {
        $shortClassName = substr(strrchr(get_class($this), '\\'), 1);
        $identifier = $this->getReadableIdentification();
        return "$shortClassName:$identifier";
    }

    public function __clone()
    {
        $this->id = null;
    }

}
