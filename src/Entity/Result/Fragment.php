<?php

namespace App\Entity\Result;


use App\Entity\EntityFields;
use App\Entity\Result;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(indexes={@ORM\Index(name="checksum", columns={"checksum"})})
 * @ORM\Entity(readOnly=true, repositoryClass="App\Repository\Result\FragmentRepository")
 **/
class Fragment extends ResultRelatedEntity
{
    use EntityFields;

    /**
     * The result.
     *
     * @var Result
     * @ORM\ManyToOne(targetEntity="App\Entity\Result", inversedBy="fragments")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $result;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var string|null
     * @ORM\Column(type="sha256")
     */
    private $checksum;

    public function __construct(Result $result, string $type, string $content, array $info = [], int $offset = 0, int $length = 0)
    {
        parent::__construct($type, $info, $offset, $length);
        $this->result = $result;
        $this->content = $content;
        $this->checksum = hash('sha256', $content, false);
    }

    public function getResult(): Result
    {
        return $this->result;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getChecksum(): ?string
    {
        return $this->checksum;
    }
}
