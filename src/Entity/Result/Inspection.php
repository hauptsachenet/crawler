<?php

namespace App\Entity\Result;


use App\Entity\EntityFields;
use App\Entity\Result;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(readOnly=true)
 * @ORM\Table()
 */
class Inspection extends ResultRelatedEntity
{
    use EntityFields;

    /**
     * This verdict tells that the inspection toured out favourably.
     * You can give yourself a pad on the shoulder because everything is alright.
     */
    const VERDICT_OK = 'ok';

    /**
     * This verdict is given if something isn't necessarily broken but should be inspected.
     * Examples:
     * - unoptimized resources
     * - content that looks like an error
     * - suspicious content changes
     */
    const VERDICT_NOTICE = 'notice';

    /**
     * This verdict means there is a definite problem that needs to be addressed.
     * Those problems are not critical and won't make the the site unusable.
     * Examples:
     * - Way to large images/resources/pages
     * - Double inclusion of resources
     * - Possible security vulnerabilities
     * - Previously available resource not available under the same uri anymore.
     * - Duplicate content
     */
    const VERDICT_PROBLEM = 'problem';

    /**
     * If you get this something is definitely wrong.
     * Examples:
     * - Directly linked resource unavailable
     * - Status codes >= 500
     */
    const VERDICT_CRITICAL = 'critical';

    /**
     * The result.
     *
     * @var Result
     * @ORM\ManyToOne(targetEntity="App\Entity\Result", inversedBy="inspections")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $result;

    /**
     * One of the VERDICT_* constants.
     *
     * @var string
     * @ORM\Column(type="binary_string", length=15)
     */
    private $verdict;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Result")
     */
    private $relatedResults;

    public function __construct(Result $result, string $type, string $verdict, array $info = [], int $offset = 0, int $length = 0)
    {
        parent::__construct($type, $info, $offset, $length);
        $this->result = $result;
        $this->verdict = $verdict;
        $this->relatedResults = new ArrayCollection();
    }

    public function getResult(): Result
    {
        return $this->result;
    }

    public function getVerdict(): string
    {
        return $this->verdict;
    }

    public function addRelatedResult(Result $result): bool
    {
        if ($this->relatedResults->contains($result)) {
            return false;
        }

        return $this->relatedResults->add($result);
    }

    public function getRelatedResults(): array
    {
        return $this->relatedResults->toArray();
    }
}
