<?php

namespace App\Entity\Result;


use App\Entity\Result;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 */
abstract class ResultRelatedEntity
{
    const PROCESSOR_CLASS_PREFIX = 'App\\Processor\\';

    /**
     * The type of inspection.
     * This string will be used in the translation key to get the correct text displaying.
     * It is usually the class name of the Inspector.
     *
     * @var string
     * @ORM\Column(type="binary_string", length=100)
     */
    private $processor;

    /**
     * Additional information that can be used to describe the issue.
     * This array will be passed to the translation when rendering the result so make it usable for that case.
     * But it might also be used for further processing in ReferenceProcessors so also include information for that.
     *
     * @var array
     * @ORM\Column(type="json")
     */
    private $info;

    /**
     * The Position within the result body to note.
     * If length is 0 than this value is not applicable.
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $offset;

    /**
     * The length of the fragment considered for this inspection.
     * If the length is 0, then the assumption is that offset/length are not relevant.
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $length;

    public function __construct(string $processor, array $info = [], int $offset = 0, int $length = 0)
    {
        if (substr($processor, 0, strlen(self::PROCESSOR_CLASS_PREFIX)) !== self::PROCESSOR_CLASS_PREFIX) {
            throw new \RuntimeException("$processor is not within namespace: "  . self::PROCESSOR_CLASS_PREFIX);
        }

        $this->processor = substr($processor, strlen(self::PROCESSOR_CLASS_PREFIX));
        $this->info = $info;
        $this->offset = $offset;
        $this->length = $length;
    }

    abstract public function getResult(): Result;

    public function getProcessor(): string
    {
        return self::PROCESSOR_CLASS_PREFIX . $this->processor;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getBodyFragment(): string
    {
        $stream = $this->getResult()->getBody();
        $stream->seek($this->getOffset(), SEEK_SET);
        return $stream->read($this->getLength());
    }
}
