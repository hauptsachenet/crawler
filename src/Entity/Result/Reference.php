<?php

namespace App\Entity\Result;


use App\Entity\EntityFields;
use App\Entity\Result;
use Doctrine\ORM\Mapping as ORM;
use Psr\Http\Message\UriInterface;

/**
 * @ORM\Entity(readOnly=true)
 * @ORM\Table()
 */
class Reference extends ResultRelatedEntity
{
    use EntityFields;

    /**
     * The result.
     *
     * @var Result
     * @ORM\ManyToOne(targetEntity="App\Entity\Result", inversedBy="references")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $result;

    /**
     * @var UriInterface
     * @ORM\Column(type="uri")
     */
    private $uri;

    public function __construct(Result $result, UriInterface $uri, string $type, array $info = [], int $offset = 0, int $length = 0)
    {
        parent::__construct($type, $info, $offset, $length);
        $this->result = $result;
        $this->uri = $uri;
    }

    public function getResult(): Result
    {
        return $this->result;
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }
}
