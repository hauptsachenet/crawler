<?php

namespace App\Entity;


use App\Entity\Result\Fragment;
use App\Entity\Result\Inspection;
use App\Entity\Result\Reference;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use GuzzleHttp\TransferStats;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 * @ORM\Table()
 */
class Result implements ResponseInterface
{
    use EntityFields;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $project;

    /**
     * @var UriInterface
     * @ORM\Column(type="uri")
     */
    private $uri;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Result\Reference", mappedBy="result", orphanRemoval=true, cascade={"ALL"})
     */
    private $references;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Result\Inspection", mappedBy="result", orphanRemoval=true, cascade={"ALL"})
     */
    private $inspections;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Result\Fragment", mappedBy="result", orphanRemoval=true, cascade={"ALL"})
     */
    private $fragments;

    /**
     * Jobs that need to performed later.
     *
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\CrawlJob", cascade={"ALL"})
     */
    private $furtherJobs;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $statusCode;

    /**
     * @var string
     * @ORM\Column(type="text", length=1000)
     */
    private $reasonPhrase;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $headers;

    /**
     * @var StreamInterface
     * @ORM\Column(type="stream")
     */
    private $body;

    /**
     * Used to easily find line numbers in the result.
     * This is not persisted as it can easily be recomputed.
     *
     * @var array|null
     */
    private $lineNumberCache;

    /**
     * @var string
     * @ORM\Column(type="string", length=3)
     */
    private $protocolVersion;

    /**
     * Stats from the crawler, probably curls.
     *
     * @see https://www.php.net/manual/en/function.curl-getinfo.php#refsect1-function.curl-getinfo-returnvalues
     * @var array
     * @ORM\Column(type="json")
     */
    private $stats;

    /**
     * Size of the download.
     * Curl calls it size_download.
     *
     * Note that value can only save the size of files up to just over 2GB.
     * Bigger size integers make problems in php.
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $transferSize = 0;

    /**
     * Time the request took in microseconds.
     * This includes transfer time and dns lookups.
     * Curl calls it total_time.
     *
     * @var float
     * @ORM\Column(type="float")
     */
    private $totalTime = 0.0;

    /**
     * Time the dns lookup took in microseconds.
     * Curl calls it namelookup_time.
     *
     * @var float
     * @ORM\Column(type="float")
     */
    private $dnsTime = 0.0;

    /**
     * Time for connect in microseconds.
     * Curl calls it namelookup_time.
     *
     * @var float
     * @ORM\Column(type="float")
     */
    private $connectTime = 0.0;

    /**
     * Time between sending the request and receiving the response in microseconds.
     * Curl calls it pretransfer_time.
     *
     * @var float
     * @ORM\Column(type="float")
     */
    private $waitTime = 0.0;

    public function __construct(Project $project, UriInterface $uri, ResponseInterface $response, TransferStats $stats = null)
    {
        $this->project = $project;
        $this->uri = $uri;
        $this->references = new ArrayCollection();
        $this->inspections = new ArrayCollection();
        $this->fragments = new ArrayCollection();
        $this->furtherJobs = new ArrayCollection();

        $this->statusCode = $response->getStatusCode();
        $this->reasonPhrase = $response->getReasonPhrase();
        $this->headers = $response->getHeaders();
        $this->body = $response->getBody();
        $this->protocolVersion = $response->getProtocolVersion();

        // because i'm lazy: stats aren't required for tests.
        // however, the entity won't save without them so this only works in unit tests
        if ($stats !== null) {
            $this->stats = $stats->getHandlerStats();
            $this->transferSize = (int)self::getHandlerStat($stats, 'size_download');
            $this->totalTime = (float)self::getHandlerStat($stats, 'total_time');
            $this->dnsTime = (float)self::getHandlerStat($stats, 'namelookup_time');
            $this->connectTime = (float)self::getHandlerStat($stats, 'connect_time');
            $this->waitTime = (float)self::getHandlerStat($stats, 'pretransfer_time');
        } else {
            $this->transferSize = (int)$this->getBody()->getSize();
        }
    }

    private static function getHandlerStat(TransferStats $stats, string $property)
    {
        $value = $stats->getHandlerStat($property);
        if ($value === null) {
            $json = json_encode($stats->getHandlerStats(), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            throw new \RuntimeException("$property does not exist in the stats: $json");
        }

        return $value;
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    /**
     * @return Reference[]
     */
    public function getReferences(): array
    {
        return $this->references->toArray();
    }

    public function createReference(string $className, UriInterface $uri, array $info = [], int $offset = 0, int $length = 0): Reference
    {
        $reference = new Reference($this, $uri, $className, $info, $offset, $length);
        $this->references->add($reference);
        return $reference;
    }

    /**
     * @return Inspection[]
     */
    public function getInspections(): array
    {
        return $this->inspections->toArray();
    }

    /**
     * A shortcut to create an inspection.
     *
     * @param string $className
     * @param string $verdict
     * @param array $info
     * @param int $offset
     * @param int $length
     *
     * @return Inspection
     */
    public function createInspection(string $className, string $verdict, array $info = [], int $offset = 0, int $length = 0): Inspection
    {
        $inspection = new Inspection($this, $className, $verdict, $info, $offset, $length);
        $this->inspections->add($inspection);
        return $inspection;
    }

    /**
     * @param string $className
     *
     * @return Inspection[]
     */
    public function getInspectionsByInspector(string $className): array
    {
        $result = [];
        foreach ($this->getInspections() as $inspection) {
            if (!is_a($inspection->getProcessor(), $className, true)) {
                continue;
            }

            $result[] = $inspection;
        }
        return $result;
    }

    /**
     * @return Fragment[]
     */
    public function getFragments(): array
    {
        return $this->fragments->toArray();
    }

    /**
     * @param string $className
     * @param string $content
     * @param array $info
     * @param int $offset
     * @param int $length
     *
     * @return Fragment
     */
    public function createFragment(string $className, string $content, array $info = [], int $offset = 0, int $length = 0): Fragment
    {
        $fragment = new Fragment($this, $className, $content, $info, $offset, $length);
        $this->fragments->add($fragment);
        return $fragment;
    }

    /**
     * @param CrawlJob $job
     *
     * @return bool
     */
    public function addFurtherJob(CrawlJob $job): bool
    {
        if ($this->furtherJobs->contains($job)) {
            return false;
        }

        foreach ($this->getFurtherJobs() as $furtherJob) {
            if ($furtherJob->getChecksum() === $job->getChecksum()) {
                return false;
            }
        }

        return $this->furtherJobs->add($job);
    }

    /**
     * @return CrawlJob[]
     */
    public function getFurtherJobs(): array
    {
        return $this->furtherJobs->toArray();
    }


    /**
     * Retrieves all message header values.
     *
     * The keys represent the header name as it will be sent over the wire, and
     * each value is an array of strings associated with the header.
     *
     *     // Represent the headers as a string
     *     foreach ($message->getHeaders() as $name => $values) {
     *         echo $name . ": " . implode(", ", $values);
     *     }
     *
     *     // Emit headers iteratively:
     *     foreach ($message->getHeaders() as $name => $values) {
     *         foreach ($values as $value) {
     *             header(sprintf('%s: %s', $name, $value), false);
     *         }
     *     }
     *
     * While header names are not case-sensitive, getHeaders() will preserve the
     * exact case in which headers were originally specified.
     *
     * @return string[][] Returns an associative array of the message's headers. Each
     *     key MUST be a header name, and each value MUST be an array of strings
     *     for that header.
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Gets the body of the message.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody(): StreamInterface
    {
        return $this->body;
    }

    /**
     * Converts a line number to a byte range that can be used in References and Inspections.
     *
     * @param int $lineNumber
     *
     * @return array
     */
    private function _convertLineNumberToRange(int $lineNumber): array
    {
        if ($this->lineNumberCache === null) {
            preg_match_all('#^.*$#imu', $this->getBody(), $matches, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);
            $this->lineNumberCache = [];
            foreach ($matches[0] as $lineNo => [$content, $offset]) {
                $this->lineNumberCache[$lineNo + 1] = [$offset, strlen($content)];
            }
        }

        if (!isset($this->lineNumberCache[$lineNumber])) {
            throw new \OutOfRangeException("Line number $lineNumber does not exist in $this.");
        }

        return $this->lineNumberCache[$lineNumber];
    }

    /**
     * Converts a line numbers to a byte range that can be used in References and Inspections.
     *
     * @param int $lineNumber
     * @param int ...$lineNumbers
     *
     * @return array
     */
    public function convertLineNumberToRange(int $lineNumber, int ...$lineNumbers): array
    {
        $result = $this->_convertLineNumberToRange($lineNumber);
        foreach ($lineNumbers as $lineNumber) {
            $nextResult = $this->_convertLineNumberToRange($lineNumber);

            $offsetDifference = $nextResult[0] - $result[0];
            if ($offsetDifference < 0) {
                $result[1] += $offsetDifference;
            }

            $lengthDifference = $nextResult[1] + $offsetDifference - $result[1];
            if ($lengthDifference > 0) {
                $result[1] += $lengthDifference;
            }
        }
        return $result;
    }

    /**
     * Retrieves the HTTP protocol version as a string.
     *
     * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
     *
     * @return string HTTP protocol version.
     */
    public function getProtocolVersion(): string
    {
        return $this->protocolVersion;
    }

    /**
     * Return an instance with the specified HTTP protocol version.
     *
     * The version string MUST contain only the HTTP version number (e.g.,
     * "1.1", "1.0").
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new protocol version.
     *
     * @param string $version HTTP protocol version
     *
     * @return static
     */
    public function withProtocolVersion($version): void
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Checks if a header exists by the given case-insensitive name.
     *
     * @param string $name Case-insensitive header field name.
     *
     * @return bool Returns true if any header names match the given header
     *     name using a case-insensitive string comparison. Returns false if
     *     no matching header name is found in the message.
     */
    public function hasHeader($name): bool
    {
        foreach ($this->getHeaders() as $thisName => $values) {
            if (strcasecmp($name, $thisName) === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieves a message header value by the given case-insensitive name.
     *
     * This method returns an array of all the header values of the given
     * case-insensitive header name.
     *
     * If the header does not appear in the message, this method MUST return an
     * empty array.
     *
     * @param string $name Case-insensitive header field name.
     *
     * @return string[] An array of string values as provided for the given
     *    header. If the header does not appear in the message, this method MUST
     *    return an empty array.
     */
    public function getHeader($name): array
    {
        $result = [];

        foreach ($this->getHeaders() as $thisName => $values) {
            if (strcasecmp($name, $thisName) === 0) {
                foreach ($values as $value) {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieves a comma-separated string of the values for a single header.
     *
     * This method returns all of the header values of the given
     * case-insensitive header name as a string concatenated together using
     * a comma.
     *
     * NOTE: Not all header values may be appropriately represented using
     * comma concatenation. For such headers, use getHeader() instead
     * and supply your own delimiter when concatenating.
     *
     * If the header does not appear in the message, this method MUST return
     * an empty string.
     *
     * @param string $name Case-insensitive header field name.
     *
     * @return string A string of values as provided for the given header
     *    concatenated together using a comma. If the header does not appear in
     *    the message, this method MUST return an empty string.
     */
    public function getHeaderLine($name)
    {
        return implode(',', $this->getHeader($name));
    }

    /**
     * Return an instance with the provided value replacing the specified header.
     *
     * While header names are case-insensitive, the casing of the header will
     * be preserved by this function, and returned from getHeaders().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new and/or updated header and value.
     *
     * @param string $name Case-insensitive header field name.
     * @param string|string[] $value Header value(s).
     *
     * @return static
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withHeader($name, $value)
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Return an instance with the specified header appended with the given value.
     *
     * Existing values for the specified header will be maintained. The new
     * value(s) will be appended to the existing list. If the header did not
     * exist previously, it will be added.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new header and/or value.
     *
     * @param string $name Case-insensitive header field name to add.
     * @param string|string[] $value Header value(s).
     *
     * @return static
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withAddedHeader($name, $value)
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Return an instance without the specified header.
     *
     * Header resolution MUST be done without case-sensitivity.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that removes
     * the named header.
     *
     * @param string $name Case-insensitive header field name to remove.
     *
     * @return static
     */
    public function withoutHeader($name)
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Return an instance with the specified message body.
     *
     * The body MUST be a StreamInterface object.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return a new instance that has the
     * new body stream.
     *
     * @param StreamInterface $body Body.
     *
     * @return static
     * @throws \InvalidArgumentException When the body is not valid.
     */
    public function withBody(StreamInterface $body)
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Gets the response status code.
     *
     * The status code is a 3-digit integer result code of the server's attempt
     * to understand and satisfy the request.
     *
     * @return int Status code.
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Return an instance with the specified status code and, optionally, reason phrase.
     *
     * If no reason phrase is specified, implementations MAY choose to default
     * to the RFC 7231 or IANA recommended reason phrase for the response's
     * status code.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * updated status and reason phrase.
     *
     * @link http://tools.ietf.org/html/rfc7231#section-6
     * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     *
     * @param int $code The 3-digit integer result code to set.
     * @param string $reasonPhrase The reason phrase to use with the
     *     provided status code; if none is provided, implementations MAY
     *     use the defaults as suggested in the HTTP specification.
     *
     * @return static
     * @throws \InvalidArgumentException For invalid status code arguments.
     */
    public function withStatus($code, $reasonPhrase = '')
    {
        throw new \LogicException("Not implemented!");
    }

    /**
     * Gets the response reason phrase associated with the status code.
     *
     * Because a reason phrase is not a required element in a response
     * status line, the reason phrase value MAY be null. Implementations MAY
     * choose to return the default RFC 7231 recommended reason phrase (or those
     * listed in the IANA HTTP Status Code Registry) for the response's
     * status code.
     *
     * @link http://tools.ietf.org/html/rfc7231#section-6
     * @link http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
     * @return string Reason phrase; must return an empty string if none present.
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

    public function getStats(): ?array
    {
        return $this->stats;
    }

    public function getTransferSize(): int
    {
        return $this->transferSize;
    }

    public function getTotalTime(): float
    {
        return $this->totalTime;
    }

    public function getDnsTime(): float
    {
        return $this->dnsTime;
    }

    public function getConnectTime(): float
    {
        return $this->connectTime;
    }

    public function getWaitTime(): float
    {
        return $this->waitTime;
    }
}
