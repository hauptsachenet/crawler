<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\JobQueueBundle\Entity\Job;
use Psr\Http\Message\UriInterface;

/**
 * @ORM\Entity(readOnly=true, repositoryClass="App\Repository\CrawlJobRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uri", columns={"uri", "command", "project_id"})})
 */
class CrawlJob
{
    use EntityFields;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $project;

    /**
     * @var UriInterface
     * @ORM\Column(type="uri")
     */
    private $uri;

    /**
     * @var string
     * @ORM\Column(type="binary_string", length=20)
     */
    private $command;

    /**
     * @var Job
     * @ORM\OneToOne(targetEntity="JMS\JobQueueBundle\Entity\Job", cascade={"ALL"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $job;

    public function __construct(Project $project, UriInterface $uri, string $command)
    {
        $this->project = $project;
        $this->uri = $uri;
        $this->command = $command;
        $this->job = new Job($command, [(string)$uri], true, 'crawler');
    }

    public function getChecksum(): string
    {
        return hash('sha256', implode('.', [
            spl_object_hash($this->project),
            (string)$this->uri,
            $this->command,
        ]));
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function getJob(): Job
    {
        return $this->job;
    }
}
