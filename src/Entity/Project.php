<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Psr\Http\Message\UriInterface;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\EntityListeners({"App\EventListener\ProjectEntityListener"})
 */
class Project
{
    use EntityFields;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $name;

    /**
     * @var UriInterface
     * @ORM\Column(type="uri")
     */
    private $startingPoint;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getStartingPoint(): UriInterface
    {
        return $this->startingPoint;
    }

    public function setStartingPoint(UriInterface $startingPoint): void
    {
        $this->startingPoint = $startingPoint;
    }
}
