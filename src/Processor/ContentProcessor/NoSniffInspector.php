<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class NoSniffInspector implements ContentProcessorInterface
{
    /**
     * @param Result $result
     */
    public function process(Result $result): void
    {
        if ($result->getTransferSize() === 0) {
            return;
        }

        $headerValue = $result->getHeaderLine('X-Content-Type-Options');
        $hasNoSniff = strcasecmp($headerValue, 'nosniff') === 0;
        $verdict = $hasNoSniff ? Inspection::VERDICT_OK : Inspection::VERDICT_PROBLEM;
        $result->createInspection(get_class($this), $verdict, ['header' => $headerValue]);
    }
}
