<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result;
use GuzzleHttp\Psr7\Uri;

class HeaderLocationFinder implements ContentProcessorInterface
{
    /**
     * @param Result $result
     */
    public function process(Result $result): void
    {
        foreach ($result->getHeader('Location') as $location) {
            $result->createReference(get_class($this), new Uri($location));
        }
    }
}
