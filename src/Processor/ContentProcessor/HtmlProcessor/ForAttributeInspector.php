<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class ForAttributeInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $xpath = new \DOMXPath($dom);
        $elementsWithForAttribute = $xpath->query('//*[@for]');

        /** @var \DOMElement $element */
        foreach ($elementsWithForAttribute as $element) {
            $forId = $element->getAttribute('for');

            $forElement = $dom->getElementById($forId);
            if (!$forElement instanceof \DOMElement) {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['element' => $element->tagName, 'value' => $forId, 'target' => null],
                    ...$result->convertLineNumberToRange($element->getLineNo())
                );
                continue;
            }

            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_OK,
                ['element' => $element->tagName, 'value' => $forId, 'target' => $forElement->tagName],
                ...$result->convertLineNumberToRange($element->getLineNo())
            );
        }
    }
}
