<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class AriaByInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $this->inspectAttribute('labelledby', $result, $dom);
        $this->inspectAttribute('describedby', $result, $dom);
    }

    private function inspectAttribute(string $attribute, Result $result, \DOMDocument $dom)
    {
        $xpath = new \DOMXPath($dom);
        /** @var \DOMElement $labeledElement */
        foreach ($xpath->query("//*[@aria-$attribute]") as $labeledElement) {
            $id = $labeledElement->getAttribute("aria-$attribute");

            $targetElement = $dom->getElementById($id);
            if (!$targetElement instanceof \DOMElement) {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['aria' => $attribute, 'value' => $id, 'text' => null],
                    ...$result->convertLineNumberToRange($labeledElement->getLineNo())
                );
                continue;
            }

            $text = trim($targetElement->textContent);
            if ($text === '') {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['aria' => $attribute, 'value' => $id, 'text' => $text],
                    ...$result->convertLineNumberToRange($labeledElement->getLineNo())
                );
                continue;
            }

            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_OK,
                ['aria' => $attribute, 'value' => $id, 'text' => $text],
                ...$result->convertLineNumberToRange($labeledElement->getLineNo())
            );
        }
    }
}
