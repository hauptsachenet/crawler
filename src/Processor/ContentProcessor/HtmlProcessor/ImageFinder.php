<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use App\Utility\UriUtility;

class ImageFinder implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        /** @var \DOMElement $imgOrSrc */
        foreach ($this->findImgAndSource($dom) as $imgOrSrc) {
            $sourceCounter = 0;

            if (strcasecmp($imgOrSrc->tagName, 'img') === 0 &&  $imgOrSrc->hasAttribute('src')) {
                $sourceCounter += count($this->parseSrc($imgOrSrc, $result));
            }

            if ($imgOrSrc->hasAttribute('srcset')) {
                $sourceCounter += count($this->parseSrcset($imgOrSrc, $result));
            }

            if ($sourceCounter === 0) {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['tag' => $imgOrSrc->tagName, 'attr' => null, 'mod' => null],
                    ...$result->convertLineNumberToRange($imgOrSrc->getLineNo())
                );
            }
        }
    }

    /**
     * @param \DOMElement $imgOrSrc
     * @param Result $result
     *
     * @return array
     */
    private function parseSrc(\DOMElement $imgOrSrc, Result $result): array
    {
        $src = $imgOrSrc->getAttribute('src');
        $uri = UriUtility::createUri($src, $result);
        if ($uri) {
            $info = ['tag' => $imgOrSrc->tagName, 'attr' => 'src', 'mod' => null];
            $range = $result->convertLineNumberToRange($imgOrSrc->getLineNo());
            $result->createReference(get_class($this), $uri, $info, ...$range);
            return [$uri];
        } else {
            // TODO figure out what kind of link this is and possibly warn since mailto in an image is not good
            return [];
        }
    }

    /**
     * @param \DOMElement $imgOrSrc
     * @param Result $result
     *
     * @return array
     */
    private function parseSrcset(\DOMElement $imgOrSrc, Result $result): array
    {
        $srcset = $imgOrSrc->getAttribute('srcset');
        $uris = [];

        foreach (explode(',', $srcset) as $src) {
            if (!preg_match('#^\s*(\S+)(?:\s+(\d+[xw]))?\s*$#', $src, $match)) {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['tag' => $imgOrSrc->tagName, 'attr' => 'srcset', 'mod' => null],
                    ...$result->convertLineNumberToRange($imgOrSrc->getLineNo())
                );

                // return instantly since this srcset is broken
                return $uris;
            }

            $uri = UriUtility::createUri($match[1], $result);
            if ($uri) {
                $info = ['tag' => $imgOrSrc->tagName, 'attr' => 'srcset', 'mod' => $match[2] ?? null];
                $range = $result->convertLineNumberToRange($imgOrSrc->getLineNo());
                $result->createReference(get_class($this), $uri, $info, ...$range);
                return [$uri];
            }
        }
    }

    /**
     * Finds all img tags and source tags that are inside picture elements.
     *
     * @param \DOMDocument $dom
     *
     * @return iterable
     */
    private function findImgAndSource(\DOMDocument $dom): iterable
    {
        /** @var \DOMElement $img */
        foreach ($dom->getElementsByTagName('img') as $img) {
            yield $img;
        }

        /** @var \DOMElement $picture */
        foreach ($dom->getElementsByTagName('picture') as $picture) {
            foreach ($picture->childNodes as $childNode) {
                if (!$childNode instanceof \DOMElement) {
                    continue;
                }

                if (strcasecmp($childNode->nodeName, 'source') !== 0) {
                    continue;
                }

                yield $childNode;
            }
        }
    }
}
