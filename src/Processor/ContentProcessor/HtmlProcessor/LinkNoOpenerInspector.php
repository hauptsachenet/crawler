<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use App\Utility\TokenListUtility;
use App\Utility\UriUtility;

class LinkNoOpenerInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $xpath = new \DOMXPath($dom);
        $links = $xpath->query('//a[@href][@target="_blank"]');

        /** @var \DOMElement $link */
        foreach ($links as $link) {
            $uri = UriUtility::createUri($link->getAttribute('href'), $result);
            if (!$uri) {
                continue;
            }

            // if this link goes to the current page ignore it
            if (UriUtility::isSameBaseUri($result->getUri(), $uri)) {
                continue;
            }

            // is target blank
            $relAttribute = $link->getAttribute('rel');
            $hasNoOpener = TokenListUtility::contains('noopener', $relAttribute);

            $result->createInspection(
                get_class($this),
                $hasNoOpener ? Inspection::VERDICT_OK : Inspection::VERDICT_PROBLEM,
                ['uri' => (string)$uri, 'host' => $uri->getHost(), 'rel' => $relAttribute],
                ...$result->convertLineNumberToRange($link->getLineNo())
            );
        }
    }
}
