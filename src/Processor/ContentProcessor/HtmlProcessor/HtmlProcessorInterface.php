<?php


namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result;

interface HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom);
}
