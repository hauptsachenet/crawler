<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class DuplicateIdInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $xpath = new \DOMXPath($dom);
        $elements = $xpath->query('//*[@id]');

        /** @var \DOMElement $element */
        foreach ($elements as $element) {
            $elementId = $element->getAttribute('id');
            $otherElement = $dom->getElementById($elementId);

            if ($otherElement !== $element) {
                $result->createInspection(
                    get_class($this),
                    Inspection::VERDICT_PROBLEM,
                    ['id' => $elementId],
                    ...$result->convertLineNumberToRange($element->getLineNo())
                );
            }
        }
    }
}
