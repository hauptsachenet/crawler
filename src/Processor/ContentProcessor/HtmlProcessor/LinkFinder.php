<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result;
use App\Utility\UriUtility;

class LinkFinder implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $possibleTagAttributeCombos = [
            'a' => ['href'],
            'link' => ['href'],
            'source' => ['src'],
            'video' => ['src'],
            //'form' => ['action'], // TODO forms require special handling
        ];

        foreach ($possibleTagAttributeCombos as $tagName => $attributes) {
            /** @var \DOMElement[] $tags */
            $tags = $dom->getElementsByTagName($tagName);
            foreach ($tags as $element) {
                foreach ($attributes as $attribute) {
                    $uri = UriUtility::createUri($element->getAttribute($attribute), $result);
                    if (!$uri) {
                        continue;
                    }

                    $range = $result->convertLineNumberToRange($element->getLineNo());
                    $info = ['tag' => $tagName, 'attr' => $attribute];
                    $result->createReference(get_class($this), $uri, $info, ...$range);
                }
            }
        }
    }
}
