<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use App\Utility\UriUtility;

class LinkTargetBlankInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $xpath = new \DOMXPath($dom);
        $links = $xpath->query('//a[@href]');

        /** @var \DOMElement $link */
        foreach ($links as $link) {
            $uri = UriUtility::createUri($link->getAttribute('href'), $result);
            if (!$uri) {
                continue;
            }

            // if this link goes to the current page ignore it
            if (UriUtility::isSameBaseUri($result->getUri(), $uri)) {
                continue;
            }

            $targetAttribute = $link->getAttribute('target');
            $hasTargetBlank = $targetAttribute === '_blank';

            $result->createInspection(
                get_class($this),
                $hasTargetBlank ? Inspection::VERDICT_OK : Inspection::VERDICT_NOTICE,
                ['uri' => (string)$uri, 'host' => $uri->getHost(), 'target' => $targetAttribute],
                ...$result->convertLineNumberToRange($link->getLineNo())
            );
        }
    }
}
