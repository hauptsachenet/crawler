<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result;
use App\Repository\Result\FragmentRepository;

class DuplicateContentInspector implements HtmlProcessorInterface
{
    private const THRESHOLD = 50;
    private const LOCAL_DOCUMENT_ELEMENTS = ['h1', 'h2', 'h3', 'p', 'title', 'div', 'form', 'ul', 'ol'];
    private const CROSS_DOCUMENT_ELEMENTS = ['h1', 'h2', 'h3', 'p', 'title'];

    /**
     * @var FragmentRepository
     */
    private $fragmentRepository;

    public function __construct(FragmentRepository $fragmentRepository)
    {
        $this->fragmentRepository = $fragmentRepository;
    }

    public function process(Result $result, \DOMDocument $dom)
    {
        /** @var \DOMNode[] $elements */
        $elements = $dom->getElementsByTagName('*');

        $fragments = [];

        foreach ($elements as $element) {
            if (!in_array(strtolower($element->nodeName), self::LOCAL_DOCUMENT_ELEMENTS)) {
                continue;
            }

            $content = preg_replace('#\s+#', ' ', trim($element->textContent));
            if (mb_strlen($content) < self::THRESHOLD) {
                continue;
            }

            $fragment = $result->createFragment(
                get_class($this),
                $content,
                ['tagName' => $element->nodeName],
                ...$result->convertLineNumberToRange($element->getLineNo(), $element->lastChild->getLineNo())
            );

            // check within this document for already existing fragments
            if (isset($fragments[$fragment->getChecksum()])) {
                $result->createInspection(
                    get_class($this),
                    Result\Inspection::VERDICT_PROBLEM,
                    ['tagName' => $element->nodeName, 'text' => $fragment->getContent()],
                    ...$result->convertLineNumberToRange($element->getLineNo(), $element->lastChild->getLineNo())
                );
            } else {
                $fragments[$fragment->getChecksum()] = $fragment;
            }

            // since there is almost always repeated navigation and footer,
            // i limit the duplication check to specific elements
            if (!in_array(strtolower($element->nodeName), self::CROSS_DOCUMENT_ELEMENTS, true)) {
                continue;
            }

            // check within other documents for fragments
            $similarFragments = $this->fragmentRepository->findByChecksum($fragment->getChecksum());
            foreach ($similarFragments as $similarFragment) {
                $inspection = $result->createInspection(
                    get_class($this),
                    Result\Inspection::VERDICT_PROBLEM,
                    ['tagName' => $element->nodeName, 'text' => $fragment->getContent()],
                    ...$result->convertLineNumberToRange($element->getLineNo(), $element->lastChild->getLineNo())
                );
                $inspection->addRelatedResult($similarFragment->getResult());
            }
        }
    }
}
