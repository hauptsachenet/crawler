<?php

namespace App\Processor\ContentProcessor\HtmlProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class RobotsInspector implements HtmlProcessorInterface
{
    public function process(Result $result, \DOMDocument $dom)
    {
        $metaTags = $dom->getElementsByTagName('meta');

        /** @var \DOMElement $metaTag */
        foreach ($metaTags as $metaTag) {
            $name = $metaTag->getAttribute('name');
            if (!preg_match('#^robots$|bot$#i', $name)) {
                continue;
            }

            $content = $metaTag->getAttribute('content');
            if (!preg_match('#noindex|nofollow#i', $name)) {
                continue;
            }

            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_NOTICE,
                ['name' => $name, 'content' => $content],
                ...$result->convertLineNumberToRange($metaTag->getLineNo())
            );
        }
    }
}
