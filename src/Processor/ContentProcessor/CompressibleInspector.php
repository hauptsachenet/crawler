<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use Symfony\Component\Console\Helper\Helper;

class CompressibleInspector implements ContentProcessorInterface
{
    const SIZE_THRESHOLD = 16 * 1024 * 1024;
    const FACTOR_THRESHOLD = 0.7;

    /**
     * @param Result $result
     */
    public function process(Result $result): void
    {
        $originalSize = intval($result->getTransferSize());
        if ($originalSize > self::SIZE_THRESHOLD) {
            return;
        }

        $compressedSize = strlen(gzcompress($result->getBody()));
        $couldBeCompressed = $originalSize * self::FACTOR_THRESHOLD > $compressedSize;

        $result->createInspection(
            get_class($this),
            $couldBeCompressed ? Inspection::VERDICT_NOTICE : Inspection::VERDICT_OK,
            [
                'size' => $originalSize,
                'size_formatted' => Helper::formatMemory($originalSize),
                'full' => $result->getBody()->getSize(),
                'full_formatted' => Helper::formatMemory($result->getBody()->getSize()),
                'compressed' => $compressedSize,
                'compressed_formatted' => Helper::formatMemory($compressedSize),
                'type' => $result->getHeaderLine('Content-Type'),
            ]
        );
    }
}
