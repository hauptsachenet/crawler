<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use Symfony\Component\Console\Helper\Helper;

class ImageInspector implements ContentProcessorInterface
{
    const MAX_SIZE = 64 * 1024 * 1024;

    /**
     * @inheritDoc
     */
    public function process(Result $result): void
    {
        $isImage = preg_match('#^image/#i', $result->getHeaderLine('Content-Type'));
        if (!$isImage) {
            return;
        }


        $size = $result->getBody()->getSize();
        $info = [
            'size' => $size,
            'sizeFormatted' => Helper::formatMemory($size),
            'oversize' => $size > self::MAX_SIZE,
            'mime' => $result->getHeaderLine('Content-Type'),
        ];

        if ($info['oversize']) {
            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_PROBLEM,
                $info
            );
            return;
        }

        // this inspector can't deal with svg's
        $size = getimagesizefromstring($result->getBody(), $imageInfo);
        if (!is_array($size)) {
            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_PROBLEM,
                $info
            );
            return;
        }

        $info['width'] = $size[0];
        $info['height'] = $size[1];
        $info['type'] = $size[2];
        $info['mime'] = $size['mime'] ?? null;
        $info['channels'] = $size['channels'] ?? null;
        $info['bits'] = $size['bits'] ?? null;

        $result->createInspection(
            get_class($this),
            Inspection::VERDICT_OK,
            $info
        );
    }
}
