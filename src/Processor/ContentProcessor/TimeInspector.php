<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class TimeInspector implements ContentProcessorInterface
{
    /**
     * @inheritDoc
     */
    public function process(Result $result): void
    {
        $waitTime = $result->getWaitTime();
        if ($waitTime > 10.0) {
            $verdict = Inspection::VERDICT_PROBLEM;
        } else if ($waitTime > 1.0) {
            $verdict = Inspection::VERDICT_NOTICE;
        } else {
            $verdict = Inspection::VERDICT_OK;
        }

        $info = ['wait_time' => $waitTime, 'total_time' => $result->getTotalTime()];
        $result->createInspection(get_class($this), $verdict, $info);
    }
}
