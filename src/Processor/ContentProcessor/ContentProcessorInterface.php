<?php


namespace App\Processor\ContentProcessor;


use App\Entity\Result;

/**
 * This interface must be implemented by processors that inspect the http body of a request.
 * They are generally run directly after the request has finished.
 *
 * They are supposed to create inspections and references.
 * References are later processed by ReferenceProcessors.
 */
interface ContentProcessorInterface
{
    /**
     * @param Result $result
     */
    public function process(Result $result): void;
}
