<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;

class RobotsInspector implements ContentProcessorInterface
{
    /**
     * @param Result $result
     */
    public function process(Result $result): void
    {
        $headerLine = $result->getHeaderLine('X-Robots-Tag');
        if (preg_match('#noindex|nofollow#i', $headerLine)) {
            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_NOTICE,
                ['headerName' => 'X-Robots-Tag', 'headerValue' => $headerLine]
            );
        }
    }
}
