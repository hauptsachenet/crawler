<?php

namespace App\Processor\ContentProcessor;


use App\Entity\Result\Inspection;
use App\Entity\Result;
use App\Processor\ContentProcessor\HtmlProcessor\HtmlProcessorInterface;
use Symfony\Component\Config\Util\Exception\XmlParsingException;
use Symfony\Component\Console\Helper\Helper;

class HtmlProcessor implements ContentProcessorInterface
{
    const MAX_SIZE = 16 * 1024 * 1024;
    const OK_SIZE = 128 * 1024;

    /**
     * @var iterable|HtmlProcessorInterface[]
     */
    private $htmlProcessors;

    public function __construct(iterable $htmlProcessors)
    {
        $this->htmlProcessors = $htmlProcessors;
    }

    /**
     * @param Result $result
     */
    public function process(Result $result): void
    {
        $isHtmlDocument = preg_match('#^(?:text|application)/x?html(?=\\+|;|$)#i', $result->getHeaderLine('Content-Type'));
        if (!$isHtmlDocument) {
            return;
        }

        $size = $result->getBody()->getSize();
        $sizeInfo = ['size' => $size, 'sizeFormatted' => Helper::formatMemory($size)];

        if ($size > self::MAX_SIZE) {
            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_PROBLEM,
                $sizeInfo
            );
            return;
        }

        if ($size < 10) {
            $result->createInspection(
                get_class($this),
                $result->getStatusCode() >= 300 ? Inspection::VERDICT_OK : Inspection::VERDICT_PROBLEM,
                $sizeInfo
            );
            return;
        }

        $result->createInspection(
            get_class($this),
            $size > self::OK_SIZE ? Inspection::VERDICT_NOTICE : Inspection::VERDICT_OK,
            $sizeInfo
        );

        try {
            $dom = $this->createDom($result->getBody());

            foreach ($this->htmlProcessors as $htmlProcessor) {
                $htmlProcessor->process($result, $dom);
            }
        } catch (XmlParsingException $e) {
            $result->createInspection(
                get_class($this),
                Inspection::VERDICT_PROBLEM,
                ['error' => $e->getMessage()]
            );
            return;
        }
    }

    private function createDom(string $content)
    {
        $internalErrors = libxml_use_internal_errors(true);
        $disableEntities = libxml_disable_entity_loader(true);
        libxml_clear_errors();

        $dom = new \DOMDocument();
        if (!$dom->loadHTML($content, LIBXML_NONET | (\defined('LIBXML_COMPACT') ? LIBXML_COMPACT : 0))) {
            libxml_disable_entity_loader($disableEntities);

            throw new XmlParsingException(implode("\n", static::getXmlErrors($internalErrors)));
        }

        libxml_use_internal_errors($internalErrors);
        libxml_disable_entity_loader($disableEntities);

        return $dom;
    }

    protected static function getXmlErrors($internalErrors)
    {
        $errors = [];
        foreach (libxml_get_errors() as $error) {
            $errors[] = sprintf('[%s %s] %s (in %s - line %d, column %d)',
                LIBXML_ERR_WARNING == $error->level ? 'WARNING' : 'ERROR',
                $error->code,
                trim($error->message),
                $error->file ?: 'n/a',
                $error->line,
                $error->column
            );
        }

        libxml_clear_errors();
        libxml_use_internal_errors($internalErrors);

        return $errors;
    }
}
