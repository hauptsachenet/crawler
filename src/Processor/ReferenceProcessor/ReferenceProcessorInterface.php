<?php


namespace App\Processor\ReferenceProcessor;


use App\Entity\Result\Reference;
use App\Entity\Result;

/**
 * These kinds of processors will be run on references between requests.
 * They will have the references and inspections of ContentProcessors available to them.
 *
 * It will be guaranteed that the documents on both ends of the request
 * are downloaded and processed by content processors.
 * However: other documents within both documents may not be loaded and should not be used.
 *
 * You can add inspections to both results if you want but think about where it makes most sense.
 * A too large image is the fault of the document where it is embedded, not a fault of the image.
 *
 * Make sure your don't really inspect the content of the page as the same result might be processed A LOT
 * based on how often it is referenced. Prefer creating a ContentProcessor that creates a meaningless inspection
 * the using it's inspection in a reference processor that then comes to a conclusion based on that.
 */
interface ReferenceProcessorInterface
{
    public function process(Reference $reference, Result $sourceResult, Result $targetResult);
}
