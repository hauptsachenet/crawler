<?php

namespace App\Processor\ReferenceProcessor;


use App\Entity\Result;
use App\Entity\Result\Reference;
use App\Processor\ContentProcessor\HtmlProcessor\ImageFinder;
use App\Processor\ContentProcessor\ImageInspector;

class ImageTagSizeInspector implements ReferenceProcessorInterface
{
    public function process(Reference $reference, Result $sourceResult, Result $targetResult)
    {
        // only care about image references
        if (!is_a($reference->getProcessor(), ImageFinder::class, true)) {
            return;
        }

        $inspections = $targetResult->getInspectionsByInspector(ImageInspector::class);
        foreach ($inspections as $inspection) {
            $image = $inspection->getInfo();
            $imgReference = $reference->getInfo();

            if (isset($image['size']) && $image['size'] > 512 * 1024) {
                $sourceResult->createInspection(
                    get_class($this),
                    Result\Inspection::VERDICT_NOTICE,
                    $image + $imgReference,
                    $reference->getOffset(),
                    $reference->getLength()
                );
            }

            // if the dimensions of the image are unknown than it can't be further inspected
            if (empty($image['width']) || empty($image['height'])) {
                continue;
            }

            // if the image tag had a size modifier (eg 2x in a srcset) i won't get into that... yet
            // - 2x must double the expected max size
            // - 1000w would bin the expected width to that value
            if (!empty($imgReference['mod'])) {
                continue;
            }

            $isOverDimensioned = $image['width'] * $image['height'] > 1500 * 1500;
            $sourceResult->createInspection(
                get_class($this),
                $isOverDimensioned ? Result\Inspection::VERDICT_NOTICE : Result\Inspection::VERDICT_OK,
                $image + $imgReference,
                $reference->getOffset(),
                $reference->getLength()
            );
        }
    }
}
