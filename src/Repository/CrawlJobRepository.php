<?php

namespace App\Repository;


use App\Entity\CrawlJob;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CrawlJobRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CrawlJob::class);
    }

    public function findExistingJob(CrawlJob $crawlJob): ?CrawlJob
    {
        if (!$this->_em->contains($crawlJob->getProject())) {
            return null;
        }

        $qb = $this->createQueryBuilder('job');

        $qb->andWhere('job.uri = :uri');
        $qb->setParameter('uri', $crawlJob->getUri());

        $qb->andWhere('job.project = :project');
        $qb->setParameter('project', $crawlJob->getProject());

        $qb->andWhere('job.command = :command');
        $qb->setParameter('command', $crawlJob->getCommand());

        try {
            $query = $qb->getQuery();
            $query->setLockMode(LockMode::PESSIMISTIC_WRITE);
            return $query->getOneOrNullResult();
        } catch (TransactionRequiredException $e) {
            throw new \RuntimeException("You need to be in a transaction if you want to safely find/create jobs.", 0, $e);
        }
    }

}
