<?php

namespace App\Repository\Result;


use App\Entity\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class FragmentRepository
 * @method Result\Fragment[] findByChecksum(string $checksum)
 */
class FragmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result\Fragment::class);
    }
}
