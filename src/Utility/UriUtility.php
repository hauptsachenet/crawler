<?php

namespace App\Utility;


use App\Entity\Result;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\UriResolver;
use Psr\Http\Message\UriInterface;

class UriUtility
{
    /**
     * Checks if both uris are on the same host.
     *
     * @param UriInterface $uri1
     * @param UriInterface $uri2
     *
     * @return bool
     */
    public static function isSameBaseUri(UriInterface $uri1, UriInterface $uri2): bool
    {
        return $uri1->getScheme() === $uri2->getScheme()
            && $uri1->getHost() === $uri2->getHost()
            && $uri1->getPort() === $uri2->getPort();
    }

    /**
     * Creates an uri from a link string.
     * Special uri's like mailto:, JavaScript:, base64 will be ignored
     *
     * @param string $content
     * @param Result $resource
     *
     * @return UriInterface|null
     */
    public static function createUri(string $content, Result $resource): ?UriInterface
    {
        // skip mailto, javascript or other uri-schemas that are not http or ftp
        if (preg_match('#^(?!https?|ftp)[^/:]+:#', $content)) {
            return null;
        }

        return UriResolver::resolve($resource->getUri(), new Uri($content));
    }
}

