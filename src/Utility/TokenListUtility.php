<?php

namespace App\Utility;


/**
 * Handle space separated lists as are common in html documents.
 */
class TokenListUtility
{
    public static function parse(string $tokens): array
    {
        preg_match_all('#\S+#', $tokens, $matches, PREG_PATTERN_ORDER);
        return $matches[0];
    }

    public static function contains(string $needle, string $tokens)
    {
        $pattern = '#(?<=^|\s)' . preg_quote($needle, '#') . '(?=\s|$)#';
        return preg_match($pattern, $tokens);
    }
}
