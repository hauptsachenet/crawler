<?php

namespace App\Command;


use App\Service\Crawler;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProjectCrawlCommand extends AbstractProjectJobCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Crawler
     */
    private $crawler;

    public function __construct(EntityManagerInterface $entityManager, Crawler $crawler)
    {
        parent::__construct($entityManager);
        $this->entityManager = $entityManager;
        $this->crawler = $crawler;
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('project:crawl');
        $this->setDescription('Crawls a single url.');
        $this->addArgument('uri', InputArgument::REQUIRED, "The uri to crawl.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = $this->getProject($input);
        $uri = new Uri($input->getArgument('uri'));

        $this->entityManager->beginTransaction();
        $output->write("Crawl $uri... ");
        $result = $this->crawler->crawl($project, $uri);
        $output->writeln("done.");

        $output->write("Persisting... ");
        $this->entityManager->persist($result);
        $this->entityManager->flush();
        $this->entityManager->commit();
        $output->writeln("done.");
    }
}
