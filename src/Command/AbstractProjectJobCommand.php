<?php

namespace App\Command;


use App\Entity\CrawlJob;
use App\Entity\Project;
use App\Repository\CrawlJobRepository;
use App\Service\Crawler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

abstract class AbstractProjectJobCommand extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CrawlJobRepository
     */
    private $crawlJobRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->crawlJobRepository = $entityManager->getRepository(CrawlJob::class);
    }

    protected function configure()
    {
        $this->setHidden(true);
        $this->addOption('jms-job-id', null, InputOption::VALUE_REQUIRED, "Jms job id.");
        $this->addOption('project', null, InputOption::VALUE_REQUIRED, "Project id.");
    }

    protected function getProject(InputInterface $input): ?Project
    {
        if ($input->getOption('project')) {
            $project = $this->entityManager->find(Project::class, $input->getOption('project'));
            if (!$project instanceof Project) {
                $type = is_object($project) ? get_class($project) : gettype($project);
                throw new \RuntimeException("Expected " . Project::class . ", got $type");
            }

            return $project;
        }

        if ($input->getOption('jms-job-id')) {
            $job = $this->crawlJobRepository->findOneBy(['job' => $input->getOption('jms-job-id')]);
            if ($job instanceof CrawlJob) {
                return $job->getProject();
            }
        }

        throw new \RuntimeException("You either need to specify --project or --jms-job-id");
    }
}
