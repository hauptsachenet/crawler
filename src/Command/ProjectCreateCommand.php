<?php

namespace App\Command;


use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProjectCreateCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    protected function configure()
    {
        $this->setName('project:create');
        $this->setDescription('Creates a new Project');

        $this->addArgument('name', InputArgument::REQUIRED, 'The name of the project.');
        $this->addArgument('startingPoint', InputArgument::REQUIRED, 'The first url to crawl.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = new Project();
        $project->setName($input->getArgument('name'));
        $project->setStartingPoint(new Uri($input->getArgument('startingPoint')));

        $violationList = $this->validator->validate($project);
        if ($violationList->count() > 0) {
            /** @var ConstraintViolationInterface $violation */
            foreach ($violationList as $violation) {
                $output->writeln("{$violation->getPropertyPath()}:\t{$violation->getMessage()}");
            }

            return 1;
        }

        $output->write("Persisting... ");
        $this->entityManager->persist($project);
        $this->entityManager->flush();
        $output->write("done, the new project id is <info>{$project->getId()}</info>.");
    }

}
