<?php

namespace App\Command;


use App\Entity\Result;
use App\Service\Crawler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProjectPostProcessCommand extends AbstractProjectJobCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Crawler
     */
    private $crawler;

    public function __construct(EntityManagerInterface $entityManager, Crawler $crawler)
    {
        parent::__construct($entityManager);
        $this->entityManager = $entityManager;
        $this->crawler = $crawler;
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('project:postprocess');
        $this->setDescription('Does post processing.');
        $this->addArgument('uri', InputArgument::REQUIRED, "The uri to crawl.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = $this->getProject($input);
        $resultRepository = $this->entityManager->getRepository(Result::class);
        $result = $resultRepository->findOneBy(['project' => $project, 'uri' => $input->getArgument('uri')]);
        $this->crawler->postProcess($result);
        $this->entityManager->persist($result);
        $this->entityManager->flush();
    }
}
