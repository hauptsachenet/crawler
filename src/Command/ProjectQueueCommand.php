<?php

namespace App\Command;


use App\Entity\Project;
use App\Service\CrawlQueue;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProjectQueueCommand extends Command
{
    /**
     * @var CrawlQueue
     */
    private $queue;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(CrawlQueue $queue, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->queue = $queue;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setName('project:queue');
        $this->setDescription('queues a url within a project');

        $this->addArgument('project', InputArgument::REQUIRED, 'Project id');
        $this->addArgument('uri', InputArgument::REQUIRED, 'uri');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = $this->entityManager->find(Project::class, $input->getArgument('project'));
        $uri = new Uri($input->getArgument('uri'));

        $job = $this->queue->createCrawlJob($project, $uri);

        $output->write("Persisting... ");
        $this->entityManager->persist($job);
        $this->entityManager->flush();
        $output->writeln("done. Created job {$job->getId()}.");
    }

}
