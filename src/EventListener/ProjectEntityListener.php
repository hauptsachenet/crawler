<?php

namespace App\EventListener;


use App\Entity\Project;
use App\Service\CrawlQueue;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ProjectEntityListener
{
    /**
     * @var CrawlQueue
     */
    private $crawlQueue;

    public function __construct(CrawlQueue $crawlQueue)
    {
        $this->crawlQueue = $crawlQueue;
    }

    public function prePersist(Project $project, LifecycleEventArgs $args)
    {
        $job = $this->crawlQueue->createCrawlJob($project, $project->getStartingPoint());

        // since we are in preFlush, we just need to add this job to the persistence
        $entityManager = $args->getEntityManager();
        if (!$entityManager->contains($job)) {
            $entityManager->persist($job);
        }
    }
}
